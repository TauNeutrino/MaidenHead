#include "../hdr/MaidenHead.hpp"
#include <cctype>
#include <iostream>
#include <string>
#include <type_traits>

// Member Functions
bool MaidenHead::GIS::isDouble(std::string test) {
  for (char c : test) {
    if (!std::isdigit(c) && !std::isalnum(c)) {
      return false;
    } else {
      return true;
    }
  }
  return false;
}

bool MaidenHead::GIS::isLat(std::string test) {
  // Test range of lat
  if (isDouble(test) && std::stod(test) >= -90.0 && std::stod(test) <= 90.0) {
    return true;
  }
  return false;
}

bool MaidenHead::GIS::isLon(std::string test) {
  // Code to test validity

  // Test range of lon
  if (isDouble(test) && std::stod(test) >= -180.0 && std::stod(test) <= 180.0) {
    return true;
  }
  return false;
}
// Current code task
bool MaidenHead::GIS::isLocator(std::string test) {
  for (char c : test) {
    if (!std::isalnum(c)) {
      return false;
    }
  }
  return true;
}
