//-------------------------------------------
// Project MaidenHead
//-------------------------------------------
#include "../hdr/MaidenHead.hpp"
#include <cstdlib>
#include <iostream>
#include <iterator>
#include <ostream>
#include <string>
int main(int argc, char const *argv[]) {
  MaidenHead::GIS gis;

#ifdef debug_status
  // Print out user input for debugging purposes
  for (int i = 0; i < argc;) {
    D("User Input", argv[i]);
    i++;
  }
#endif

  // Parse user input
  /* Valid formats:
          ./MaidenHead <LAT> <LON>  will convert to locator
          ./MaidenHead <LAT Initial> <LON Initial> <LAT Final> <LON Final> will
     calculate GCR
          ./MaidenHead <LAT Initial> <LON Initial> <LOCATOR>  will calculate GCR
          ./MaidenHead <LOCATOR> <LOCATOR> will calculate GCR
  */
  std::vector<std::string> userInput;
  for (int i = 0; i < argc; i++) {
    userInput.push_back(argv[i]);
  }
  // Test userInputs
  if (argc == 3 && gis.isLat(userInput[1]) && gis.isLon(userInput[2])) {
    D("Lat :", userInput[1]);
    D("Lon: ", userInput[2]);
    std::string s = userInput[1];
    gis.setLat(std::stod(userInput[1]));
    gis.setLon(std::stod(userInput[2]));
  } else {
    std::cout << "Not a valid Lat or Long" << std::endl;
  }
  if (argc == 5 && gis.isLat(userInput[1]) && gis.isLon(userInput[2]) &&
      gis.isLat(userInput[3]) && gis.isLon(userInput[4])) {
    D("Lat Initial :", userInput[1]);
    D("Lon Initial : ", userInput[2]);
    D("Lat Final :", userInput[3]);
    D("Lon Final : ", userInput[4]);
    gis.setLatInitial(std::stod(userInput[1]));
    gis.setLonInitial(std::stod(userInput[2]));
    gis.setLatFinal(std::stod(userInput[3]));
    gis.setLonFinal(std::stod(userInput[4]));
  }
  if(argc == 4 && gis.isLat(userInput[1]) && gis.isLon(userInput[2]) && gis.isLocator(userInput[3])){
    D("Lat Initial :", userInput[1]);
    D("Lon Initial : ", userInput[2]);
    D("Locator :", userInput[3]);
    gis.setLatInitial(std::stod(userInput[1]));
    gis.setLonInitial(std::stod(userInput[2]));
    gis.setLoc(userInput[3]);
  }



  return 0;

} // main()
// EOF
