# MaidenHead
## DESCRIPTION
### By: TauNeutrino
- This project's repository: [git@codeberg.org:TauNeutrino/MaidenHead.git]
- Edit this README

--- 

Directory Tree
<pre>
/home/tau/Projects/MaidenHead
|-- hdr
|   `-- MaidenHead.hpp
|-- src
|   `-- main.cpp
|-- makefile
`-- README.md

2 directories, 4 files
</pre>