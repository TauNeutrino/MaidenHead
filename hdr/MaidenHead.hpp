//-------------------------------------------
// MAIDENHEAD_HPP
//-------------------------------------------
#ifndef MAIDENHEAD_HPP
#define MAIDENHEAD_HPP
// -- Debug Status --//
#include <sstream>
#include <string>
#include <vector>
#define debug_status true
// Macro to print variable details
#if debug_status
#define D(x, y)                                                                \
  std::cout << x << "\t"                                                       \
            << "\033[;33m" << y << "\t\033[0m"                                 \
            << " in function [" << __PRETTY_FUNCTION__ << "] line {"           \
            << __LINE__ << "} of"                                              \
            << "[ " << __FILE__ << "]\t@ " << __TIME__ << " on " << __DATE__   \
            << std ::endl;
#else
#define D(...)
#endif
#define Yo "\033[;33m"
#define Yc "\033[0m"
namespace MaidenHead {
#define PI 3.141592653589793238462643383279
// Maidenhead GIS class
class GIS {
protected:
  double m_lat = 0.0;
  double m_lon = 0.0;
  std::string m_loc = "";
  std::string m_locInitial = "";
  std::string m_locFinal = "";
  double m_latInitial = 0.0;
  double m_lonInitial = 0.0;
  double m_latFinal = 0.0;
  double m_lonFinal = 0.0;
  bool isDouble(std::string);

public:
  inline double rad2Deg(double rad) { return (rad * 180 / PI); };
  inline double deg2Rad(double deg) { return (deg * PI / 180); };
  std::vector<std::string> userInput;
  bool isLocator(std::string);
  bool isLat(std::string);
  bool isLon(std::string);
  inline void setLat(double lat) { m_lat = lat; }
  inline double getLat() { return m_lat; }
  inline void setLon(double lon) { m_lon = lon; }
  inline double getLon() { return m_lon; }
  inline void setLatInitial(double lat) { m_latInitial = lat; }
  inline double getLatInitial() { return m_latInitial; }
  inline void setLonInitial(double lon) { m_lonInitial = lon; }
  inline double getLonInitial() { return m_lonInitial; }
  inline void setLatFinal(double lat) { m_latFinal = lat; }
  inline double getLatFinal() { return m_latFinal; }
  inline void setLonFinal(double lon) { m_lonFinal = lon; }
  inline double getLonFinal() { return m_lonFinal; }
  inline void setLoc(std::string loc) { m_loc = loc; }
  inline std::string getLoc() { return m_loc; }
};

} // namespace MaidenHead
#endif // MAIDENHEAD_HPP
